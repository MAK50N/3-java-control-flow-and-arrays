package com.example.task11;

import java.util.Arrays;

public class Task11Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        int[] arr = {7, 5, 9};
        swap(arr);
        System.out.println(java.util.Arrays.toString(arr));
         */
    }

    static void swap(int[] arr) {
        if (arr != null && arr.length != 0) {
            int min_ = Integer.MAX_VALUE;
            int ind = 0;
            for (int j = 0; j < arr.length; j++) {
                if (arr[j] <= min_) {
                    min_ = arr[j];
                    ind = j;
                }
            }
            int tmp = arr[0];
            arr[0] = arr[ind];
            arr[ind] = tmp;
        }
    }

}